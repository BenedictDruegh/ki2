def get_reward_table(world_map:list)->list:
    # empty 2d table with each elem as an empty dict
    reward_table = [[{} for _ in range(4)] for _ in range(4)]

    # set None values for edges
    for x in range(4):
        reward_table[0][x]['north'] = None
        reward_table[3][x]['south'] = None

    for y in range(4):
        reward_table[y][0]['west'] = None
        reward_table[y][3]['east'] = None


    for y, row in enumerate(world_map):
        for x, cell_content in enumerate(row):

            # not grab or climb by default
            reward_table[y][x]['grab'] = None
            reward_table[y][x]['climb'] = None

            if cell_content in ['W', 'P']: # wumpus or pit
                set_adjacent_reward_values(reward_table, x, y, -1000)

            elif cell_content == 'G': # gold 
                set_adjacent_reward_values(reward_table, x, y, 1000)
                reward_table[y][x]['grab'] = 1000

            elif cell_content == 'L': # ladder (only appears after clearing)
                set_adjacent_reward_values(reward_table, x, y, 1000)
                reward_table[y][x]['climb'] = 1000

            else: # normal cell
                set_adjacent_reward_values(reward_table, x, y, -1) # avoid unnecessary moves

    return reward_table


def get_init_world_map()->list:
    return [['_', '_', '_', 'P'],
            ['W', 'G', 'P', '_'],
            ['_', '_', '_', '_'],
            ['_', '_', 'P', '_']]


def is_map_cleared(world_map:list)->bool:
    for row in world_map:
        for cell in row:
            if cell == 'G': # gold remains, not cleared
                return False

    return True


def get_init_q_table()->list:
    cell_value = {
        "north": 0,
        "south": 0,
        "west": 0,
        "east": 0,
        "grab": 0,
        "climb": 0
    }
    return [[cell_value.copy() for _ in range(4)] for _ in range(4)]



def set_adjacent_reward_values(reward_table:list, x:int, y:int, reward_value:float):
    # northern cell to this cell
    if y > 0:
        reward_table[y - 1][x]['south'] = reward_value

    # southern cell to this cell
    if y < 3:
        reward_table[y + 1][x]['north'] = reward_value

    # western cell to this cell
    if x > 0:
        reward_table[y][x - 1]['east'] = reward_value

    # eastern cell to this cell
    if x < 3:
        reward_table[y][x + 1]['west'] = reward_value

if __name__ == "__main__":
    table = get_reward_table(get_init_world_map())
    for row in table:
        for cell in row:
            print(cell, end = " | ")

        print()